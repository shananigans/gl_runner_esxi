#!/bin/bash

#####################################################################
# Check for packages that need installed before setting up the venv
#####################################################################
PKG_LIST=""
for PKG in gcc python3-pip python3-devel openssl-devel python3-libselinux python3-virtualenv libvirt-devel; do
    rpm -qi $PKG > /dev/null 2>&1
    if [ $? != 0 ]; then
        PKG_LIST="${PKG_LIST} $PKG"
    fi
done

if [ "${PKG_LIST}" != "" ]; then
    echo "Installing packages required for molecule"
    echo "  - See: https://molecule.readthedocs.io/en/latest/installation.html"
    echo
    sudo yum install -y ${PKG_LIST}
fi

###################
# Set up the venv
###################

if [ ! -d ./molecule_venv ]; then
    echo "Creating ./molecule_venv"
    virtualenv ./molecule_venv

    echo "Upgrading setuptools and pip"
    . molecule_venv/bin/activate
    pip install --upgrade setuptools
    pip install --upgrade pip

    echo "Install molecule"
    pip install -r ./molecule-requirements.txt
   
    deactivate
fi


#############################
# Print out some useful info
#############################

echo "New role example:      molecule init role my-new-role --driver-name docker"
echo "Existing role example: molecule init scenario -r my-role-name --driver-name podman  (from with role dir)"
