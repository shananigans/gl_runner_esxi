# gl_runner_esxi

Playbooks for setting up a gitlab runner with nested esxi hypervisors for testing ova images.

## Completed roles

* install_libvirt: Installs hypervisor tools and enables nested virt.
* base_esxi_image: Customizes esxi iso with kickstart file and generates base esxi qcow2 image from it.


## TODO

roles needed:
* gl_runner_config: Install the gitlab runner, set up proper libvirt permissions and configure for pre/post node playbooks. 
* {spinup,destroy}_esxi_node: Used by gitlab runner pre/post scripts to spin up and tear down esxi nodes from a base image snapshot for testing.(Use CI_CONCURRENT_ID for libvirt vm & net suffix for concurrent runs.)
* deploy_test_image: Download ova/ovf files from provided url, spin up vm onto esxi node and generate ansible inv file for test playbooks.

