import os
import time

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


def test_custom_iso_existance(host):
    assert host.file("/var/lib/libvirt/images/esxi-custom.iso").exists

def test_esxi_base_image_existance(host):
    assert host.file("/var/lib/libvirt/images/esxi-base.qcow2").exists

def test_iso_contents(host):
    host.run("sudo mkdir /mnt/isotest")
    host.run("sudo mount /var/lib/libvirt/images/esxi-custom.iso /mnt/isotest")
    try:
        assert host.file("/mnt/isotest/ks_cust.cfg").exists
        assert 'KS_CUST.CFG' in host.file("/mnt/isotest/boot.cfg").content_string
    finally:
        host.run("sudo umount /mnt/isotest")
        host.run("sudo rmdir /mnt/isotest")
        
def test_qemu_image_format(host):
    cmdout = host.run("sudo qemu-img info /var/lib/libvirt/images/esxi-base.qcow2")
    assert 'file format: qcow2' in cmdout.stdout
