import os
import time

import testinfra.utils.ansible_runner

testinfra_hosts = testinfra.utils.ansible_runner.AnsibleRunner(
    os.environ['MOLECULE_INVENTORY_FILE']).get_hosts('all')


# We installed the full virtulization or virt host
# package group. Doing some spot checks here.
def test_libvirt_packages_are_installed(host):
    test_pkgs = [
      'libvirt',
      'libvirt-client',
    ]
    for curpkg in test_pkgs:
        curpkg = host.package(curpkg)
        assert curpkg.is_installed

# libvirtd defaults to timing out after 120 secs and 
# re-enables on socket activity.  We will need to prime
# it with a command before checking the status.
def test_libvirt_is_running_and_enabled(host):

    # Prime the service and wait a few seconds
    host.run("sudo virsh list")
    time.sleep(3)

    # Test the status
    libvirt_svc = host.service("libvirtd")
    assert libvirt_svc.is_enabled
    assert libvirt_svc.is_running
  

def test_nested_virt(host):
    nested_enabled = False
    for curmod in ['kvm_intel', 'kvm_amd']:
        curfile = "/sys/module/{}/parameters/nested".format(curmod)
        if os.path.isfile(curfile): 
            cmd = host.run("cat /sys/module/{}/parameters/nested".format(curmod))
            if cmd.stdout.rstrip() == "Y" or cmd.stdout.rstrip() == '1':
                nested_enabled = True

    assert nested_enabled
